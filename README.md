## 外网sdk访问API网关工具

- php 版本大于 7.1
- 需要开启 extension=php_openssl.dll 模块

1. 拷贝 congo-open-sdk 文件夹到项目中
2. 修改配置文件 `congo-open-sdk/conf.php`
3. 引入 `congo-open-sdk/OpenApi.php` 就可以使用网管工具，具体例子参考 `demo/demo.php`

## 统一支付平台接口

1. 修改配置文件 `congo-open-sdk/conf.php`
2. 相关代码在 `congo-open-sdk/Pay.php` 中，具体例子参考 `demo/pay.php`
