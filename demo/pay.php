<?php
require_once('../congo-open-sdk/Pay.php');

$uri = trim($_SERVER['REQUEST_URI'], '/');
if (strpos($uri, 'pay/cashStagePayQuery') === 0) {
    $params = $_GET;
    if ($params === null) {
        $data = file_get_contents('php://input');
        $params = json_decode(urldecode($data), JSON_OBJECT_AS_ARRAY);
    }

    // $params = array(
    //     'merchantId' => '888073157340070',
    //     'merchantOrderId' => '1212312',
    // );

    $result = \CongoOpenSDK\Pay::getCashStage($params);

    header('Content-Type:application/json; charset=utf-8');
    exit($result);
} else if (strpos($uri, 'pay/cashStageWithdrawal') === 0) {

    $data = file_get_contents('php://input');
    $params = json_decode(urldecode($data), JSON_OBJECT_AS_ARRAY);

    // $datetime = new \DateTime;
    // $params = array(
    //     'amount' => '1',
    //     'merchantId' => '888073157340070',
    //     'merchantOrderId' => '2021042901957',
    //     'notifyUrl' => 'http://127.0.0.1/pay/callbackUrl',
    //     'orderDate' => $datetime->format('Ymd'),
    //     'openid' => '123456',
    //     'payOrg' => 'WX_ENTERPRISE_PAY',
    //     'productDesc' => 'for test',
    // );

    $result = \CongoOpenSDK\Pay::cashStageWithdrawal($params);

    header('Content-Type:application/json; charset=utf-8');
    exit($result);
} else if (strpos($uri, 'pay/cashStageRefund') === 0) {

    $data = file_get_contents('php://input');
    $params = json_decode(urldecode($data), JSON_OBJECT_AS_ARRAY);

    // $params = array(
    //     'amount' => '1',
    //     'merchantId' => '888073157340070',
    //     'refundnotifyurl' => 'http://127.0.0.1/pay/callbackUrl',
    //     'orderId' => '2021042901957',
    //     'payNo' => '2021042901957',
    // );

    $result = \CongoOpenSDK\Pay::cashStageRefund($params);

    header('Content-Type:application/json; charset=utf-8');
    exit($result);
} else if (strpos($uri, 'pay/cashStage') === 0) {

    $data = file_get_contents('php://input');
    $params = json_decode(urldecode($data), JSON_OBJECT_AS_ARRAY);

    // $params = array(
    //     'productName' => 'created for test',
    //     'merchantId' => '888073157340070',
    //     'merchantOrderId' => '2021042901957',
    //     'amount' => '1',
    //     'callbackUrl' => 'http://127.0.0.1/pay/callbackUrl',
    //     'notifyUrl' => 'http://127.0.0.1/pay/callbackUrl',
    // );

    $result = \CongoOpenSDK\Pay::createCashStage($params);

    header('Content-Type:application/json; charset=utf-8');
    exit($result);
} else {
    exit('success');
}
?>
