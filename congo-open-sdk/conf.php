<?php
return array(
    // 平台获取的应用的appkey
    'APP_KEY' => '',
    // 平台获取的应用的secret
    'APP_SECRET' => '',
    // 开放平台网关
    'GATEWAY' => 'https://gateway.gcongo.onebuygz.com/api',
    // 生成的应用私钥
    'PRIVATE_KEY' => '',
    // 在开放平台的服务公钥
    'SERVER_PUBLIC_KEY' => '',
    // 平台设置的应用公钥
    'PUBLIC_KEY' => '',

    // 统一支付平台分配的商户 key
    'PAY_MERCHANT_KEY' => '',

    'PAY_PATH_CREATE_CASH_STAGE' => '/uppv2cashdesk/uppv2cashdesk/cashStage',
    'PAY_PATH_GET_CASH_STAGE' => '/uppv2cashdesk/uppv2cashdesk/cashStagePayQuery',
    'PAY_PATH_REFUND' => '/uppv2cashdesk/uppv2cashdesk/cashStageRefund',
    'PAY_PATH_WITHDRAWAL' => '/uppv2cashdesk/uppv2cashdesk/cashStageWithdrawal',
)
?>
